import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/Billetera',
    name: 'Billetera',
    component: () => import(/* webpackChunkName: "about" */ '../views/Billetera.vue')
  }
  ,
  {
    path: '/Cuentas',
    name: 'Cuentas',
    component: () => import(/* webpackChunkName: "about" */ '../views/Cuentas.vue')
  }
  ,
  {
    path: '/Registrarse',
    name: 'Registrarse',
    component: () => import(/* webpackChunkName: "about" */ '../views/Registrarse.vue')
  },
  {
    path: '/OlvidoClave',
    name: 'OlvidoClave',
    component: () => import(/* webpackChunkName: "about" */ '../views/OlvidoClave.vue')
  }
  ,
  {
    path: '/Pruebas',
    name: 'Pruebas',
    component: () => import(/* webpackChunkName: "about" */ '../views/Pruebas.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
